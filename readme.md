# KAF #

## Setting up your machine ##

You must be running a 64-bit Linux distribution and must have installed some packages to build
KAF. Google recommends using [Ubuntu](http://www.ubuntu.com/download/desktop) for
this and provides instructions for setting up the system (with Ubuntu-specific commands) on
[the Android Open Source Project website](https://source.android.com/source/initializing.html#setting-up-a-linux-build-environment).

Once you have set up your machine according to the instructions by Google, return here and carry
on with the rest of the instructions.

## Grabbing the source ##

[Repo](http://source.android.com/source/developing.html) is a tool provided by Google that
simplifies using [Git](http://git-scm.com/book) in the context of the Android source.

### Installing Repo ###

```bash
# Make a directory where Repo will be stored and add it to the path
$ mkdir ~/.bin
$ PATH=~/.bin:$PATH

# Download Repo itself
$ curl https://storage.googleapis.com/git-repo-downloads/repo > ~/.bin/repo

# Make Repo executable
$ chmod a+x ~/.bin/repo
```

### Initializing Repo ###

```bash
# Create a directory for the source files
# You can name this directory however you want, just remember to replace
# WORKSPACE with your directory for the rest of this guide.
# This can be located anywhere (as long as the fs is case-sensitive)
$ mkdir WORKSPACE
$ cd WORKSPACE

# Install Repo in the created directory
# Use a real name/email combination, if you intend to submit patches
$ repo init -u https://gitlab.com/kalil-caf/manifest -b caf-9.0 -m manifest.xml
```

### Downloading the source tree ###

This is what you will run each time you want to pull in upstream changes. Keep in mind that on your
first run, it is expected to take a while as it will download all the required Android source files
and their change histories.

```bash
# Let Repo take care of all the hard work
#
# The -j# option specifies the number of concurrent download threads to run.
# 4 threads is a good number for most internet connections.
# You may need to adjust this value if you have a particularly slow connection.
$ repo sync -j4 -c --no-tags
```

## Building ##

```bash
$ . build/make/envsetup.sh
# device= your device
# type= build type. it can be eng, userdebug or user. Normally, you want this to be userdebug
$ lunch custom_device-type
$ mka bacon
```

## Quirks ##

Because this is essentially pure CAF, there are some quirks:

1- CAF uses more classes from qti-telephony-common. If your OEM bundled proprietary shit on these
you will have problems. Using the OSS one is recommended

2- We cant use soong namespaces and pathmap for the hals. Some repos need to know the HAL path or
need some of the HAL packages. One example is libsurfaceflinger, that needs the display hal, and
other example is vendor/qcom/opensource/commonsys/bluetooth_ext/system_bt_ext/btif that needs to
know the bluetooth hal path.
The problem with this is that they use soong, so we cant just use an variable. To work arund this
issue, KAF doesnt use the regular HAL selection method, but instead, generates an manifest with
the hals, and syncs at buildtime, at the normal path. It can be overrided by just placing your
hal manifest (or a dummy one) in .repo/local_manifests/hals.xml. Only 8996, and 8998 hals are ready, so,
unless the phone you are building uses them, clone them yourself, and add a dummy manifest to the
path.
